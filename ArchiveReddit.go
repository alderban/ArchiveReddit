package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"github.com/jzelinskie/geddit"
)

// Function to "handle" errors by quitting the program after printing the error message.
func check(err error, message string) {
	if err != nil {
		fmt.Printf("%s: %s", message, err.Error())
		os.Exit(0)
	}
}

// Create structure and global variable for config files.
type Conf struct {
	Clientkey    string
	Clientsecret string
	Username     string
	Password     string
	Archivedir   string
}

var conf Conf

func main() {
	// Parse configuration files
	userconf, err := os.UserConfigDir()
	check(err, "Error:")
	_, err = toml.DecodeFile(userconf+"/ArchiveReddit/conf.toml", &conf)
	check(err, "Configuration error")

	// Authorization
	o, err := geddit.NewOAuthSession(
		conf.Clientkey,
		conf.Clientsecret,
		"",
		"https://example.com/",
	)
	check(err, "Error authorizing")

	err = o.LoginAuth(conf.Username, conf.Password)
	check(err, "Error authorizing")

	getSavedPosts(o, "")
}

func getSavedPosts(o *geddit.OAuthSession, after string) {
	var mySavedLinks []*geddit.Submission
	var err error
	if after == "" {
		mySavedLinks, err = o.MySavedLinks(geddit.ListingOptions{Limit: 100})
	} else {
		mySavedLinks, err = o.MySavedLinks(geddit.ListingOptions{Limit: 100, After: after})
	}

	check(err, "Error fetching posts")
	for index, post := range mySavedLinks {
		savePost(post)
		if index == 99 {
			getSavedPosts(o, post.FullID)
		}
	}
}

func savePost(post *geddit.Submission) {
	var postdirname string

	postjson, err := json.Marshal(post)
	check(err, "Error converting post to json")

	s := string(filepath.Separator)
	// Truncate post dir name
	postdirname = post.Title
	if len(postdirname) > 250 {
		postdirname = postdirname[:250] + "..."
	}
	fmt.Println("Archived post:" + postdirname)
	postpath := conf.Archivedir + s + "posts" + s + post.Subreddit + s + postdirname + s + post.FullID + s

	err = os.MkdirAll(postpath, 664)
	check(err, "Error creating post directory")

	err = os.WriteFile(string(postpath+"post.json"), []byte(postjson), 664)
	check(err, "Error writing file")
}
