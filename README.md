# ArchiveReddit
A Go program and learning project for archiving Reddit posts.   
Currently only archiving your saved posts on Reddit.
## Compilation/Installation
1. Git clone this repository.
```sh
git clone <to be added>
```
2. Install [Go](https://golang.org/)
3. Go get the dependencies
```sh
go get github.com/BurntSushi/toml
go get github.com/jzelinskie/geddit
```
4. Go build the code into an executable by doing the below command in the project folder.
```sh
go build .
```

## Usage
1. Create a new script application at https://www.reddit.com/prefs/apps and name it something like ArchiveR, you can use https://example.com/ for the redirect uri.
2. Create a file called conf.toml inside a directory named ArchiveReddit in your configuration folder (On Unix $XDG_CONFIG_HOME if non-empty, else $HOME/.config. On Darwin (MacOS), $HOME/Library/Application Support. On Windows, %AppData%.).
3. Populate the configuration file like this:
```toml
Clientkey = <The client key for the application you created in step 1>
Clientsecret = <The client secret for the application you created in step 1>
Username = <Your Reddit username>
Password = <Your Reddit password>
Archivedir = <The directory you wish the client to archive the data in>
```
4. Now you can run the executable you built.

## TODO
- Automatic archiving on post link content incase of link/image post.
- Option to archive other posts than the ones you've saved on Reddit.
- Option to use without Reddit account credentials.

## Notes
- This program has only been tested on Linux.
- I am open to adding features on request or accepting contributions.
- This project fully relies on [Geddit](https://github.com/jzelinskie/geddit).