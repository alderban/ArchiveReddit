module ArchiveReddit

go 1.17

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/beefsack/go-rate v0.0.0-20200827232406-6cde80facd47 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/jzelinskie/geddit v0.0.0-20200521013404-78c28c13fba2 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/turnage/graw v0.0.0-20201204201853-a177df1b5c91 // indirect
	github.com/turnage/redditproto v0.0.0-20151223012412-afedf1b6eddb // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6 // indirect
	google.golang.org/appengine v1.4.0 // indirect
)
